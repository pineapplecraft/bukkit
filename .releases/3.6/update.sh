#!/bin/bash

RELEASE_DIR=`pwd`
PLUGINS_DIR="$RELEASE_DIR/../.."
SERVER_DIR="$PLUGINS_DIR/.."

echo "Installing custom map updates..."
WORLD_DATA_DIR=$SERVER_DIR/world/data
cp -f $PLUGINS_DIR/.maps/map_*.dat $WORLD_DATA_DIR

echo "Removing invalid dynmap tiles..."
DYNMAP_TILES_DIR=$PLUGINS_DIR/dynmap/web/tiles
rm -rf $DYNMAP_TILES_DIR/world
rm -rf $DYNMAP_TILES_DIR/world_nether