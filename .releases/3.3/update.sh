#!/bin/bash

RELEASE_DIR=`pwd`
PLUGINS_DIR="$RELEASE_DIR/../.."
SERVER_DIR="$PLUGINS_DIR/.."

echo "Installing games world."
cp -rf games $SERVER_DIR

echo "Setting up the libraries symlink."
cd $SERVER_DIR
rm -fr lib
ln -s plugins/.lib lib
cd $RELEASE_DIR