#!/bin/bash

echo "Fixing the Towny regen on plot deletion"

cd ../../Towny/data/worlds 

sed -i 's/usingPlotManagementDelete=true/usingPlotManagementDelete=false/g' *.txt
sed -i 's/usingPlotManagementMayorDelete=true/usingPlotManagementMayorDelete=false/g' *.txt
sed -i 's/usingPlotManagementRevert=true/usingPlotManagementRevert=false/g' *.txt
