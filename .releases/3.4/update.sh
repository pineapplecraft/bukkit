#!/bin/bash

RELEASE_DIR=`pwd`
PLUGINS_DIR="$RELEASE_DIR/../.."
SERVER_DIR="$PLUGINS_DIR/.."

echo "Installing custom maps..."
WORLD_DATA_DIR=$SERVER_DIR/world/data
rm -f $WORLD_DATA_DIR/idcounts.dat
rm -f $WORLD_DATA_DIR/map_*.dat
cp -f $PLUGINS_DIR/.maps/map_*.dat $WORLD_DATA_DIR