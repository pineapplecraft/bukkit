# NOTE: Rember that ANYONE can run these commands. It is up to the command
#       being aliased to prevent it from going further.

###########################################################################
## Ranking controls

*:/promote $player            = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.promote'))), die('You do not have permission to use this command'))
                                    /pex promote $player
                                    broadcast(concat(color(gold), to_upper($player), color(white), ' was promoted'))
                                <<<
*:/demote $player             = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.promote'))), die('You do not have permission to use this command'))
                                    /pex demote $player
                                    broadcast(concat(color(gold), to_upper($player), color(white), ' was demoted'))
                                <<<
*:/setrank $player $rank      = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.setrank'))), die('You do not have permission to use this command'))
                                    /pex user $player group set $rank
                                    broadcast(concat(color(gold), to_upper($player), color(white), ' was set to ', color(gold), to_upper($rank), color(white), ' rank'))
                                <<<
                                
*:/getrank $player            = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.getrank'))), die('You do not have permission to use this command'))
                                    /pex user $player group list
                                    #This commented section implements this with a simple list if the player is online.
                                    #Wasn't sure what Nat wanted, so put both the simple and more complicated checks in.
                                    #try(
                                    #    player($player) # Will throw exception if player is offline
                                    #    msg(concat(color(white), 'Current ranks for ', color(gold), to_upper($player)))
                                    #    foreach(pgroup($player), @group,
                                    #        msg(concat(color(aqua), @group))
                                    #    ), @ex,
                                    #    /pex user $player group list,
                                    #    array(PlayerOfflineException)
                                    #)
                                <<<
                                
## Special Rank Controls
## Tutor (Games world helper)
*:/tutor add $player          = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.tutor.promote'))), die('You do not have permission to use this command'))
                                    assign(@pgroup, pgroup($player))
                                    if(array_contains(@pgroup, 'tutor'), die(concat(color(gold), to_upper($player), color(white), ' is already a ', color(gold), 'Tutor', color(while), '.')))
                                    array_push(@pgroup, 'tutor')
                                    run(concat('/pex user ', $player, ' group set ', array_implode(@pgroup, ',')))
                                    broadcast(concat(color(gold), to_upper($player), color(white), ' has become a ', color(gold), 'Tutor', color(white), '.'))
                                    call_alias(sconcat('/tag add', $player, 'tutor'))
                                <<<

*:/tutor remove $player       = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.tutor.promote'))), die('You do not have permission to use this command'))
                                    assign(@pgroup, pgroup($player))
                                    if(
                                        array_contains(@pgroup, 'tutor'),
                                        run(/pex user $player group remove tutor)
                                        broadcast(concat(color(gold), to_upper($player), color(white), ' is no longer a ', color(gold), 'Tutor', color(white), '.'))
                                        call_alias(sconcat('/tag remove', $player, 'tutor')),
                                        die(concat(color(gold), to_upper($player), color(white), ' is not a ', color(gold), 'Tutor', color(while), '.'))
                                    )                                    
                                <<<

*:/tutor off                 = >>>
                                   if(and(nequals('~console', player()), not(has_permission('internal.tutor.toggle'))), die('You do not have permission to use this command'))
                                   run(/pex group tutor remove flight.on games)
                                   run(/pex group tutor remove flight.toggle games)
                               <<<

*:/tutor on                   = >>>
                                   if(and(nequals('~console', player()), not(has_permission('internal.tutor.toggle'))), die('You do not have permission to use this command'))
                                   run(/pex group tutor add flight.on games)
                                   run(/pex group tutor add flight.toggle games)
                               <<<

###########################################################################
## Quest controls 
##
## Shrines: seed, light, growth, faith, moisture

*:/nq                         = >>>
                                    if(equals('~console', player()), die('This command is unavailable from the console'))
                                    if(
                                        has_permission('internal.nq'),
                                        assign(@total_chances, 5)
                                        msg('===================================================')
                                        msg(color(red), 'WARNING: You only get ', @total_chances, ' chances to pass this test!')
                                        msg('')
                                        msg('Visit the ', color(yellow), '5', color(white), ' shrines in this area and answer the questions as you are instructed')
                                        msg('')
                                        msg('Type ', color(aqua), '/nq status', color(white), ' to see your current answers')
                                        msg('Type ', color(aqua), '/nq pray', color(white), ' when you think you have ', color(yellow), 'all', color(white), ' the answers')
                                        msg('')
                                        msg('For answers, see the ', color(aqua), '/help'),
                                        msg('Only newbies can complete the newbie quest!')
                                    )
                                <<<

*:/nq $shrine $letter         = >>>
                                    if(equals('~console', player()), die('This command is unavailable from the console'))
                                    if(
                                        has_permission('internal.nq'),
                                        msg('===================================================')
                                        assign(@prefix, concat('nq.', to_lower(player()), '.'))
                                        if(
                                            and(
                                                equals_ic(length(reg_match('seed|light|growth|faith|moisture', $shrine)), 1),
                                                equals_ic(length(reg_match('[a-d]', $letter)), 1)
                                            ),
                                            msg('You answered ', color(gold), to_upper($letter), color(white), ' at the Shrine of ', color(gold), to_upper($shrine))
                                            store_value(concat(@prefix, to_lower($shrine)), to_lower($letter)),
                                            msg('Incorrect format, please try again (type /nq for help)')
                                        ),
                                        msg('Only newbies can complete the newbie quest!')
                                    )
                                <<<

*:/nq status                  = >>>
                                    if(equals('~console', player()), die('This command is unavailable from the console'))
                                    if(
                                        has_permission('internal.nq'),
                                        msg('===================================================')
                                        assign(@prefix, concat('nq.', to_lower(player()), '.'))
                                        foreach(
                                            array('seed', 'light', 'growth', 'faith', 'moisture'), 
                                            @shrine,
                                            if(
                                                has_value(concat(@prefix, @shrine)),
                                                msg('Answered ', color(gold), to_upper(get_value(concat(@prefix, @shrine))), color(white), ' at the Shrine of ', color(gold), to_upper(@shrine)),
                                                msg(color(gray), 'Not Answered at the Shrine of ', to_upper(@shrine))
                                            )
                                        ),
                                        msg('Only newbies can complete the newbie quest!')
                                    )
                                <<<

*:/nq pray                    = >>>
                                    if(equals('~console', player()), die('This command is unavailable from the console'))
                                    if(
                                        has_permission('internal.nq'),
                                        msg('===================================================')
                                        assign(@total_chances, 5)
                                        assign(@prefix, concat('nq.', to_lower(player()), '.'))
                                        assign(@num_wrong, 0)
                                        if(
                                            nequals('b', get_value(concat(@prefix, 'seed'))),
                                            assign(@num_wrong, add(@num_wrong, 1))
                                        )
                                        if(
                                            nequals('c', get_value(concat(@prefix, 'light'))),
                                            assign(@num_wrong, add(@num_wrong, 1))
                                        )
                                        if(
                                            nequals('d', get_value(concat(@prefix, 'growth'))),
                                            assign(@num_wrong, add(@num_wrong, 1))
                                        )
                                        if(
                                            nequals('a', get_value(concat(@prefix, 'faith'))),
                                            assign(@num_wrong, add(@num_wrong, 1))
                                        )
                                        if(
                                            nequals('c', get_value(concat(@prefix, 'moisture'))),
                                            assign(@num_wrong, add(@num_wrong, 1))
                                        )
                                        if(
                                            equals(@num_wrong, 0),
                                            msg(color(5), 'The Pineapple God hears your prayer and grants you a new rank! Congratulations!')
                                            msg('')
                                            runas(~op, concat('/pex user ', player(), ' group set stone'))
                                            runas(~op, concat('/kit starter ', player()))
                                            runas(player(), '/spawn')
                                            broadcast(concat(color(gold), player(), color(white), ' has successfully passed the newbie quest!')),
                                            assign(@chances_tag, concat(@prefix, 'chances'))
                                            assign(@chances, get_value(@chances_tag))
                                            if(
                                                or(
                                                    is_null(@chances), 
                                                    gt(@chances, 1)
                                                ),
                                                if(
                                                    is_null(@chances), 
                                                    assign(@chances, subtract(@total_chances, 1))
                                                    store_value(@chances_tag, @chances),
                                                    assign(@chances, subtract(@chances, 1))
                                                    store_value(@chances_tag, @chances)
                                                )
                                                msg(color(5), 'Your prayer is not heard... ')
                                                msg('')
                                                if(
                                                    equals(@chances, 1),
                                                    msg(color(red), 'LAST CHANCE before you are automatically banned!'),
                                                    msg(color(red), 'Careful, you only have ', @chances, ' chances left!')
                                                )
                                                msg('')
                                                msg('You have ', color(red), @num_wrong, color(white), ' incorrect answers')
                                                msg('Revisit the shrines & make sure your answers are correct')
                                                msg('For more help, type: ', color(aqua), '/nq status', color(white), ' or just ', color(aqua), '/nq'),
                                                broadcast(concat(color(gold), player(), color(white), ' has failed the newbie quest!'))
                                                set_pbanned(player(), true)
                                                kick(player())
                                            )
                                        ),
                                        msg('The Pineapple God smiles at you')
                                    )
                                <<<
                                
*:/nqa reset $player          = >>>
                                    if(
                                        or(
                                            equals('~console', player()),
                                            has_permission('internal.nqa')
                                        ),
                                        set_pbanned($player, false)
                                        clear_value(concat('nq.', to_lower($player), '.chances'))
                                        call_alias(concat('/setrank ', $player, ' coal'))
                                        runas($player, '/spawn')
                                        msg(color(green), 'The newbie quest for ', color(darkgreen), $player, color(white), ' was reset'),
                                        msg(color(red), 'You do not have permission to do this')
                                    )
                                <<<
                                
###########################################################################
## Help shortcuts
*:/help                       = >>>
                                    msg(color(green), 'To generate help sheets, please type:')
                                    msg(color(aqua), '/help commands')
                                    msg(color(aqua), '/help intro')
                                    msg(color(aqua), '/help ranks')
                                    msg(color(aqua), '/help rules')
                                    msg(color(aqua), '/help warps')
                                    msg(color(aqua), '/help website')
                                <<<
*:/help $type                 = >>>
                                    msg(color(green), 'Generating help sheet(s) for ', color(gold), to_upper($type))
                                    concat('/kit help-', $type)
                                <<<
*:/commands                   = call_alias('/help commands')
*:/intro                      = call_alias('/help intro')
*:/ranks                      = call_alias('/help ranks')
*:/rules                      = call_alias('/help rules')
*:/warps                      = call_alias('/help warps')
*:/website                    = call_alias('/help website') 

## Event shortcuts
*:/event                      = /ewarp event
*:/event set                  = >>>
                                    msg(color(green), 'Event set commands (for event runners):')
                                    msg(color(aqua), '/event set warp', color(white), ': Sets the warp for /event')
                                    msg(color(aqua), '/event set respawn', color(white), ': Sets the games world respawn (can only be used in the games world)')
                                <<<
*:/event set warp             = /esetwarp event
*:/event set respawn          = /mvsetspawn

## Warping shortcuts
*:/hub                        = /ewarp hub
*:/market                     = /ewarp market
*:/mines                      = /ewarp mines
*:/nether                     = /ewarp nether
*:/origins                    = /ewarp origins
*:/quest                      = /ewarp quest
*:/residence                  = /ewarp residence
*:/survival                   = /ewarp survival
*:/tutor                      = >>>
                                    if(equals('~console', player()), die('This command can not be executed from the console.'))
                                    if(not(has_permission('internal.tutor')), die('You do not have permission to use this warp'))
                                    /ewarp tutor
                                <<<
*:/endershop                  = >>>
                                    if(equals('~console', player()), die('This command can not be executed from the console.'))
                                    if(not(has_permission('internal.endershop')), die('You do not have permission to use this warp'))
                                    /ewarp endershop
                                <<<
                                
## Misc shortcuts
*:/fly                        = /flight toggle
*:/m $                        = /msg $
*:/afk                        = /eafk

###########################################################################
## Player Tag commands
*:/tag add $player $tag       = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.tags'))), die('You do not have permission to use this command'))
                                    assign(@tagPairs, get_value('globalplayertags.tagpairs'))
                                    if(
                                        not(is_array(@tagPairs)),
                                        die('Error: Unknown tag')
                                    )
                                    if(
                                        not(array_contains(array_keys(@tagPairs), to_lower($tag))),
                                        die('Error: Unknown tag')
                                    )
                                    assign(@curTag, @tagPairs[to_lower($tag)])
                                    assign(@stored, concat('playertags.', to_lower($player)))
                                    assign(@tagArray, get_value(@stored))
                                    if(
                                        is_array(@tagArray),
                                        if(
                                            array_contains(@tagArray, @curTag),
                                            die(concat(color(gold), to_upper($player), color(white), ' already has the ', color(gold), $tag, color(white) , ' tag.')),
                                            array_push(@tagArray, @curTag)
                                        ),
                                        assign(@tagArray, array(@curTag))
                                    )
                                    run(concat('/pex user ', $player, ' prefix "{groupprefix}&b[', array_implode(@tagArray, ' '), '&b]&f"'))
                                    store_value(@stored, @tagArray)
                                    die(concat(color(gold), to_upper($player), color(white), ' now has the ', color(gold), $tag, color(white), ' tag.'))
                                <<<
                                
*:/tag remove $player $tag    = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.tags'))), die('You do not have permission to use this command'))
                                    assign(@tagPairs, get_value('globalplayertags.tagpairs'))
                                    if(
                                        not(is_array(@tagPairs)),
                                        die('Error: Unknown tag')
                                    )
                                    if(
                                        not(array_contains(array_keys(@tagPairs), to_lower($tag))),
                                        die('Error: Unknown tag')
                                    )
                                    assign(@curTag, @tagPairs[to_lower($tag)])
                                    assign(@stored, concat('playertags.', to_lower($player)))
                                    assign(@tagArray, get_value(@stored))
                                    if(
                                        is_array(@tagArray),
                                        for(
                                            assign(@i,0), lt(@i, array_size(@tagArray)), inc(@i),
                                            if(
                                                equals(@tagArray[@i], @curTag),
                                                if(
                                                    equals(array_size(@tagArray), 1),
                                                    run(concat('/pex user ', $player, ' prefix ""'))
                                                    clear_value(@stored),
                                                    array_remove(@tagArray, @i)
                                                    run(concat('/pex user ', $player, ' prefix "{groupprefix}&b[', array_implode(@tagArray, ' '), '&b]&f"'))
                                                    store_value(@stored, @tagArray)
                                                )
                                                die(concat(color(gold), to_upper($player), color(white), ' no longer has the ', color(gold), $tag, color(white), ' tag.'))
                                            )
                                        )
                                    )
                                    die(concat(color(gold), to_upper($player), color(white), ' does not have the ', color(gold), $tag, color(white), ' tag.'))
                                 <<<
                                 
*:/tag clear $player          = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.tags'))), die('You do not have permission to use this command'))
                                    run(concat('/pex user ', $player, ' prefix ""'))
                                    clear_value(concat('playertags.', to_lower($player)))
                                    die(concat(color(gold), to_upper($player), color(white), ' no longer has any tags.'))
                                <<<
                                 
*:/tag list [$page=1]         = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.tags'))), die('You do not have permission to use this command'))
                                    assign(@tagPairs, get_value('globalplayertags.tagpairs'))
                                    if(
                                        not(is_array(@tagPairs)),
                                        die('There are no tags available.')
                                    )
                                    assign(@numTags, length(@tagPairs))
                                    assign(@keys, array_keys(@tagPairs))
                                    msg(concat(color(aqua), 'Available Tags ', color(white), 'Page ', color(green), $page, color(white), ' of ', color(green), ceil(divide(@numTags, 5))))
                                    for(assign(@i, multiply(subtract($page,1), 5)), and(lt(@i, multiply($page,5)), lt(@i, @numTags)), inc(@i),
                                        msg(concat(color(white), add(@i,1), '. ' , @keys[@i], ': ', @tagPairs[@keys[@i]]))
                                    )
                                <<<
                                 
*:/tag [$=0]                  = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.tags'))), die('You do not have permission to use this command'))
                                    msg(concat(color(white), 'Usages: Modify player tags'))
                                    msg(concat(color(aqua), '/tag add PLAYER TAG', color(white), '    Add TAG to PLAYER'))
                                    msg(concat(color(aqua), '/tag remove PLAYER TAG', color(white), ' Remove TAG from PLAYER'))
                                    msg(concat(color(aqua), '/tag list [page]', color(white), '       List available tags'))
                                    msg(concat(color(aqua), '/tag clear PLAYER', color(white), '      Remove all of tags on PLAYER'))
                                <<<

## Tag admin commands (manage available tags)
*:/tagadmin add $name $color $tag  = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.tagsadmin'))), die('You do not have permission to use this command'))
                                    assign(@stored, concat('globalplayertags.tagpairs'))
                                    assign(@tagPairs, get_value(@stored))
                                    assign(@nameLower, to_lower($name))
                                    if(
                                        not(equals_ic(length(reg_match('^[0-9a-z]+$', @nameLower)), 1)),
                                        die('Name can only contain letters and numbers')
                                    )
                                    if(
                                        not(equals_ic(length(reg_match('^[0-9a-fA-F]$', $color)), 1)),
                                        die(concat('Color can only be ',
                                            color(black), '0', color(dark_blue), '1', color(dark_green), '2', color(dark_aqua), '3',
                                            color(red), '4', color(dark_purple), '5', color(gold), '6', color(gray), '7',
                                            color(dark_gray), '8', color(blue), '9', color(green), 'a', color(aqua), 'b',
                                            color(red), 'c', color(light_purple), 'd', color(yellow), 'e', color(white), 'f',)
                                        )
                                    )
                                    if(
                                        not(equals_ic(length(reg_match('^[0-9a-zA-Z|&[0-9a-fA-F]]+$', $tag)), 1)),
                                        die('Tag can only contain letters and numbers')
                                    )
                                    if(
                                        not(is_array(@tagPairs)),
                                        assign(@tagPairs, array())
                                    )
                                    assign(@newTag, concat('&', $color, $tag))
                                    array_set(@tagPairs, @nameLower, @newTag)
                                    store_value(@stored, @tagPairs)
                                    die(concat(color(green), 'Added tag ', color(white), @nameLower, ': ', @newTag))
                                <<<
                                
*:/tagadmin remove $name      = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.tagsadmin'))), die('You do not have permission to use this command'))
                                    assign(@stored, concat('globalplayertags.tagpairs'))
                                    assign(@tagPairs, get_value(@stored))
                                    if(
                                        is_array(@tagPairs),
                                        if(
                                            array_contains(array_keys(@tagPairs), to_lower($name)),
                                            if(
                                                equals(length(@tagPairs), 1),
                                                clear_value(@stored),
                                                array_remove(@tagPairs, to_lower($name))
                                                store_value(@stored, @tagPairs)
                                            )
                                            die(concat(color(red), 'Removed tag ', color(white), to_lower($name)))
                                        )
                                    )
                                    die(concat(color(white), 'There is no tag with name: ', color(aqua), to_lower($name)))
                                <<<
                                    
*:/tagadmin [$=0]             = >>>
                                    if(and(nequals('~console', player()), not(has_permission('internal.tagsadmin'))), die('You do not have permission to use this command'))
                                    msg(concat(color(white), 'Usages: Modify/View Global Tag List'))
                                    msg(concat(color(aqua), '/tagadmin add NAME COLOR TAG', color(white), ' Add NAME and TAG pair'))
                                    msg(concat(color(aqua), '/tagadmin remove NAME', color(white), '  Remove pair by NAME'))
                                    msg(concat('Color can only be ',
                                            color(black), '0', color(dark_blue), '1', color(dark_green), '2', color(dark_aqua), '3',
                                            color(red), '4', color(dark_purple), '5', color(gold), '6', color(gray), '7',
                                            color(dark_gray), '8', color(blue), '9', color(green), 'a', color(aqua), 'b',
                                            color(red), 'c', color(light_purple), 'd', color(yellow), 'e', color(white), 'f',))
                                <<<
